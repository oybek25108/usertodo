import { createApp } from 'vue'
import "./assets/scss/index.scss"
import App from './App.vue'
import './plugins/VeeValidate'
import VueTheMask from 'vue-the-mask'
import store from "./store";
import router from './router'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";

const options = {
  transition: "Vue-Toastification__fade",
  newestOnTop: false,
  position: "top-right",
  timeout: 4000,
  closeOnClick: true,
  pauseOnFocusLoss: true,
  pauseOnHover: true,
  draggable: true,
  draggablePercent: 1.42,
  showCloseButtonOnHover: true,
  hideProgressBar: true,
  closeButton: "button",
  icon: true,
  rtl: false
};

const myApp = createApp(App).use(VueTheMask).use(store).use(router).use(Toast, options);
myApp.mount('#app')
