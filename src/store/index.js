import { createStore } from 'vuex'
import {http} from "../plugins/Axios/index.js";
import useNotification from "../hooks/notification.js";

const { setResponseError } = useNotification()

const store = createStore({
  state () {
    return {
      usersList: []
    }
  },
  getters: {
    getUsersList(state) {
      return state.usersList
    },
    getUsersCount(state) {
      return state.usersList.length
    }
  },
  mutations: {
    getUsersFromLocalStorage(state) {
      if (localStorage.getItem('users-list')) {
        let usersList = JSON.parse(localStorage.getItem('users-list'));
        state.usersList = usersList
      }
    },
    addUser(state, payload) {
      state.usersList.unshift(payload)
      localStorage.setItem('users-list', JSON.stringify(state.usersList));
    },
    deleteUser(state, payload) {
      for(let i = 0; i < payload.length; i++){
        const deletedUserId = state.usersList.findIndex(user => user.id === payload[i]);
        state.usersList.splice(deletedUserId, 1);
      }
      localStorage.setItem('users-list', JSON.stringify(state.usersList));
    },
  },
  actions: {
    async getUsersData({commit}){
      try {
        if (!localStorage.getItem('users-list')) {
          const {data}  = await http.get("users");
          localStorage.setItem('users-list', JSON.stringify(data));
        }
        commit('getUsersFromLocalStorage')
      } catch (e) {
        setResponseError({error: e})
      }
    },
  }
})

export default store
