import axios from "axios";

export let baseURL = import.meta.env.VITE_API_BASE_URL;
baseURL += "/";

export const http = axios.create({
  baseURL,
})
