import { defineRule } from 'vee-validate';
import { required, email, min } from '@vee-validate/rules';
// defineRule('required', required);
defineRule('email', email);
defineRule('min', min);


defineRule('required', value => {
  if (!value || !value.length) {
    return 'This field is required';
  }
  return true;
});


defineRule('fullName', value => {
  if (!/\w+\s\w+/.test(value)) {
    return 'This field must be two words or more by space';
  }
  return true;
});
