/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    container: {
      screens: {
        sm: '600px',
        md: '728px',
        lg: '984px',
        xl: '984px',
        '2xl': '984px',
      },
    },
    // screens: {
    //   '2xl': {'max': '1535px'},
    //   // => @media (max-width: 1535px) { ... }
    //
    //   'xl': {'max': '1279px'},
    //   // => @media (max-width: 1279px) { ... }
    //
    //   'lg': {'max': '1023px'},
    //   // => @media (max-width: 1023px) { ... }
    //
    //   'md': {'max': '767px'},
    //   // => @media (max-width: 767px) { ... }
    //
    //   'sm': {'max': '639px'},
    //   // => @media (max-width: 639px) { ... }
    // },
    extend: {},
    fontSize: {
      "xs": ["10px", "10px"],
      "sm": ["14px", "20px"],
      "base": ["16px", "24px"],
      "lg": ["18px", "28px"],
      "2xl": ["32px", "40px"],
    },
    fontWeight: {
      "normal": 400,
      "medium": 500,
      "semi-bold": 600,
      "bold": 700,
      "extra-bold": 800,
    },
    colors: {
      transparent: "transparent",
      white: "#fff",
      black: {
        900: "#000000",
        700: "#171F26",
      },
      blue: {
        400: "#4577D8",
        200: "#DCE8FF",
        100: '#EFF9FC'
      },
      grey: {
        700: "#74797D",
        600: "#959595",
        300: "#F8F8FA",
      },
      teal: '#4dc0b5',
      red: "#F23D5B",
      purple: {
        600: "#9333ea"
      },
    },
  },
}
